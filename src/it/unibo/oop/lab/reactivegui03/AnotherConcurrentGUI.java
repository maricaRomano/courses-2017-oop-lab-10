package it.unibo.oop.lab.reactivegui03;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


/**
 * 
 */
public class AnotherConcurrentGUI extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = -8710276539980695794L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel display = new JLabel();
    private final JButton stop = new JButton("stop");
    private final JButton up = new JButton("up");
    private final JButton down = new JButton("down");
    /**
     * Builds a CGUI.
     */
    public AnotherConcurrentGUI() {
        super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel panel = new JPanel();
        panel.add(display);
        panel.add(up);
        panel.add(down);
        panel.add(stop);
        this.getContentPane().add(panel);
        this.setVisible(true);
        final Agent agent = new Agent();
        /**/
        new Thread(agent).start(); 
        /**/
        stop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                // TODO Auto-generated method stub
                agent.stopCounting();
            }
        });
        /**
         * make the counter go up.
         * */
        up.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                // TODO Auto-generated method stub
                agent.incrementCounting();
            }
        });
        /**
         * make the counter go down.
         * */
        down.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                // TODO Auto-generated method stub
                agent.decrementCounting();
            }
        });
    }
    /*********/
    public class Agent implements Runnable {
        private volatile boolean stop;
        private boolean up = true;
        private int counter;
        /**
         * 
         */
        public void run() {
           while (!this.stop) {
               try {
                   SwingUtilities.invokeAndWait(new Runnable() {
                       public void run() {
                           AnotherConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
                       }
                   });
                   if (this.up) {
                       this.counter++;
                   } else {
                       this.counter--;
                   }
                   Thread.sleep(100);
               } catch (InvocationTargetException | InterruptedException ex) {
                   ex.printStackTrace();
               }
           }
        }
        /**
         * make the counter go down to minus infinite.
         * 
         */ 
        public void decrementCounting() {
            this.up = false;
        }
        /**
         *make the counter go up to plus infinite.
         */
        public void incrementCounting() {
            this.up = true;
        }
        /**
         * External command to stop counting.
         */
        public void stopCounting() {
            this.stop = true;
        }
    }

}



